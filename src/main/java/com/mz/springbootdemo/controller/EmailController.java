package com.mz.springbootdemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author 盟主
 * @Description TODO..
 * @ClassName SpringBootDemo.com.mz.springbootdemo.controller.EmailController.java
 * @createTime 2024年02月21日 11:16:00
 */
@RestControllerAdvice
@RequestMapping("/email")
public class EmailController {
    
}
