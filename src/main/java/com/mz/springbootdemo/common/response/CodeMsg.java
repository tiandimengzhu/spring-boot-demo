package com.mz.springbootdemo.common.response;

import java.util.HashMap;
import java.util.Map;

/**
 * 公共返回码Code基类
 * @author 盟主
 */
public class CodeMsg {
    /**
     * 请求成功
     */
    public static final int SUCCESS = 200;
    /**
     * 服务器异常
     */
    public static final int ERROR = 500;
    /**
     * 请求的资源找不到
     */
    public static final int NOT_FOUND = 11001;
    /**
     * 非法的签名
     */
    public static final int INVALID_SIGN = 11002;
    /**
     * 参数不能为空
     */
    public static final int NULL_PARAM = 11004;
    /**
     * 非法的参数
     */
    public static final int INVALID_PARAM = 11005;
    /**
     * 重复数据
     */
    public static final int EXITING = 11006;
    /**
     * 暂无匹配数据
     */
    public static final int NO_MATCHING = 11007;
    /**
     * 无效的用户
     */
    public static final int INVALID_USER = 11009;
    /**
     * 操作失败
     */
    public static final int OPERATE_FAIL = 11010;
    /**
     * 不能操作
     */
    public static final int OPERATE_ILLEGAL = 11011;
    /**
     * 非法的网络地址
     */
    public static final int UNKNOWN_HOST = 11014;

    /**
     * 获取用户信息失败，请联系管理员.
     */
    public static final int INVALID_TOKEN_USER = 12023;

    /**
     * 敏感词
     */
    public static final int SENSITIVE_WORD = 12023;


    protected static final Map<Integer, String> MSG = new HashMap<Integer, String>();

    static {
        // 系统常用
        MSG.put(SUCCESS, "请求成功.");
        MSG.put(ERROR, "服务器异常.");

        // 账户相关
        MSG.put(NOT_FOUND, "请求资源未找到.");
        MSG.put(INVALID_SIGN, "签名非法.");
        MSG.put(INVALID_TOKEN_USER, "获取用户信息失败，请联系管理员.");
        MSG.put(NULL_PARAM, "请求参数不能为空.");
        MSG.put(INVALID_PARAM, "非法的请求参数.");
        MSG.put(EXITING, "重复的数据请求.");

        MSG.put(NO_MATCHING, "暂无匹配数据.");
        MSG.put(INVALID_USER, "非法的用户.");
        MSG.put(OPERATE_FAIL, "操作失败.");
        MSG.put(OPERATE_ILLEGAL, "非法操作.");

        // 系统通用
        MSG.put(UNKNOWN_HOST, "非法的网络地址");
        MSG.put(SENSITIVE_WORD, "您输入的内容中包含敏感词");

    }


    public static String getMsg(int errCode) {
        return MSG.get(errCode);
    }

}
