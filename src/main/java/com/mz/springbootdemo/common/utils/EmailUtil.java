package com.mz.springbootdemo.common.utils;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.ArrayList;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.extra.mail.Mail;
import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import com.sun.mail.util.MailSSLSocketFactory;
import org.apache.naming.factory.SendMailFactory;

/**
 * @author 盟主
 * @Description TODO..
 * @ClassName SpringBootDemo.com.mz.springbootdemo.common.utils.EmailUtil.java
 * @createTime 2024年02月21日 11:19:00
 */
public class EmailUtil {

    public static void sendRtmapMsg(){
        System.out.println("开始rtmap发送邮件");
        ArrayList<String> tos = CollUtil.newArrayList("467713292@qq.com");
        MailAccount mailAccount = new MailAccount();
        //邮件服务器的SMTP地址
        mailAccount.setHost("smtp.exmail.qq.com");
        //邮件服务器的SMTP端口
        mailAccount.setPort(465);
        mailAccount.setAuth(true);
        mailAccount.setStarttlsEnable(true);
        mailAccount.setSslEnable(true);
        //发件人（必须正确，否则发送失败）
        mailAccount.setFrom("service_ma1@rtmap.com");
        //用户名
        mailAccount.setUser("service_ma1@rtmap.com");
        mailAccount.setPass("***");
        String result = MailUtil.send(mailAccount, tos,"测试", "邮件来自盟主测试", false);
        System.out.println("发送rtmap邮件"+result);
    }

    public static void sendMsg(){
        System.out.println("开始发送乐高邮件");
        ArrayList<String> tos = CollUtil.newArrayList("467713292@qq.com");
        MailAccount mailAccount = new MailAccount();
        //邮件服务器的SMTP地址
        mailAccount.setHost("smtp-mail.outlook.com");
        //邮件服务器的SMTP端口
        mailAccount.setPort(587);
        mailAccount.setAuth(true);
        mailAccount.setStarttlsEnable(true);
        mailAccount.setSslEnable(false);
        mailAccount.setDebug(true);
        //发件人（必须正确，否则发送失败）
        mailAccount.setFrom("notify@legolandshanghai.cn");
        //用户名
        mailAccount.setUser("notify@legolandshanghai.cn");
        mailAccount.setPass("Luk93148");
        System.out.println(mailAccount.toString());
        String result = MailUtil.send(mailAccount, tos,"测试", "邮件来自盟主测试", false);
        System.out.println("发送乐高邮件"+result);
    }

    public static void main(String[] args) {
        //sendRtmapMsg();
        //sendTestOutLookMsg();
        sendMsg();
    }
}
