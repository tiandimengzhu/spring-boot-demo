/*
 * RT MAP, Home of Professional MAP
 * Copyright 2017 Bit Main Inc. and/or its affiliates and other contributors
 * as indicated by the @author tags. All rights reserved.
 * See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 */
package com.mz.springbootdemo.common.config;

import com.mz.springbootdemo.common.filter.XssFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

/**
 * Description: web共用配置
 * @author 盟主
 * @package WebConfiguration
 * @date 2022/2/25 10:50 上午
*/
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/doc.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/**").addResourceLocations("classpath:/templates/");
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        super.addResourceHandlers(registry);
    }
   /* @Bean
    public FilterRegistrationBean<CORSFilter> getCORSFilter(){
        FilterRegistrationBean<CORSFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new CORSFilter());
        registration.setName("CORSFilter");
        registration.addUrlPatterns("/*");
        registration.addInitParameter("cors.allowOrigin", "*");
        registration.addInitParameter("cors.supportedMethods", "OPTIONS, GET, POST, HEAD, PUT, DELETE");
        registration.addInitParameter("cors.supportedHeaders", "Accept, Origin, X-Requested-With, token, JSESSIONID, cookie, Cookie, Content-Type, Last-Modified, Authorization,entityId,userId");
        registration.addInitParameter("cors.exposedHeaders", "Set-Cookie");
        registration.addInitParameter("cors.supportsCredentials", "true");
        registration.setOrder(0);
        return registration;
    }*/

    @Bean
    public FilterRegistrationBean<XssFilter> getXssFilter(){
        FilterRegistrationBean<XssFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new XssFilter());
        registration.setName("XssFilter");
        registration.addUrlPatterns("/*");
        registration.setDispatcherTypes(EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD));
        registration.setOrder(2);
        return registration;
    }
}
